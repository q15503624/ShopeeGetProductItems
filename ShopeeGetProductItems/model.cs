﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class model
    {
        public class ItemRatingReply
        {
        }

        public class PreviewItem
        {
            public int itemid { get; set; }
            public int snapshotid { get; set; }
            public int modelid { get; set; }
        }

        public class UserRatingReply
        {
        }

        public class Extinfo
        {
            public int seller_promotion_refresh_time { get; set; }
            public int promo_source { get; set; }
            public int stockout_time { get; set; }
            public bool? has_shopee_promo { get; set; }
            public int? total_stock_for_discount { get; set; }
            public int? stock_before_discount { get; set; }
            public int? upcoming_fs_promo_id { get; set; }
            public int? seller_promotion_limit { get; set; }
        }

        public class Model
        {
            public string sku { get; set; }
            public int status { get; set; }
            public string name { get; set; }
            public int itemid { get; set; }
            public int ctime { get; set; }
            public int price { get; set; }
            public string currency { get; set; }
            public int stock { get; set; }
            public int mtime { get; set; }
            public Extinfo extinfo { get; set; }
            public int modelid { get; set; }
            public int rebate_price { get; set; }
            public int price_before_discount { get; set; }
            public int promotionid { get; set; }
        }

        public class ProductItem
        {
            public int itemid { get; set; }
            public int offer_count { get; set; }
            public string item_status { get; set; }
            public bool can_use_wholesale { get; set; }
            public int rating_bad { get; set; }
            public int promo_source { get; set; }
            public int estimated_days { get; set; }
            public int price_max_before_discount { get; set; }
            public string sku { get; set; }
            public int preview_end_time { get; set; }
            public int total_stock_for_discount { get; set; }
            public List<int> rating_count { get; set; }
            public int rating_good { get; set; }
            public bool is_cc_installment_payment_eligible { get; set; }
            public int rcount_with_image { get; set; }
            public int attr_status { get; set; }
            public bool is_non_cc_installment_payment_eligible { get; set; }
            public int sub_catid { get; set; }
            public int condition { get; set; }
            public int ctime { get; set; }
            public int price_min_sp { get; set; }
            public string name { get; set; }
            public bool show_shopee_verified_label { get; set; }
            public bool is_pre_order { get; set; }
            public bool seller_coin_setting { get; set; }
            public int seller_promotionid { get; set; }
            public int service_by_shopee_flag { get; set; }
            public int rating_normal { get; set; }
            public int bundle_deal_id { get; set; }
            public bool has_lowest_price_guarantee { get; set; }
            public double rating_star { get; set; }
            public int model_discount { get; set; }
            public string images { get; set; }
            public int price_before_discount { get; set; }
            public int upcoming_fs_promo_id { get; set; }
            public int catid { get; set; }
            public bool is_official_shop { get; set; }
            public int modelid { get; set; }
            public bool show_official_shop_label_in_title { get; set; }
            public string discount { get; set; }
            public bool is_attribute_failed { get; set; }
            public string country { get; set; }
            public List<object> attributes { get; set; }
            public int cmt_count { get; set; }
            public string image { get; set; }
            public List<string> image_list { get; set; }
            public int shopid { get; set; }
            public int pop { get; set; }
            public int rcount_with_context { get; set; }
            public bool is_shopee_verified { get; set; }
            public int price_min_before_discount_sp { get; set; }
            public List<int> cats { get; set; }
            public int liked_count { get; set; }
            public int recommend { get; set; }
            public int is_snapshot { get; set; }
            public int price_min_before_discount { get; set; }
            public int option { get; set; }
            public string brand { get; set; }
            public bool show_official_shop_label { get; set; }
            public int flag { get; set; }
            public List<object> wholesale_tier_list { get; set; }
            public int stockout_time { get; set; }
            public List<object> tier_variations_formatted { get; set; }
            public int seller_promotion_refresh_time { get; set; }
            public int snapshotid { get; set; }
            public List<object> videos { get; set; }
            public string currency { get; set; }
            public int mtime { get; set; }
            public int raw_discount { get; set; }
            public bool is_category_failed { get; set; }
            public int fs_stockout_time { get; set; }
            public int price_min { get; set; }
            public bool can_use_bundle_deal { get; set; }
            public string collect_address { get; set; }
            public int cb_option { get; set; }
            public int show_discount { get; set; }
            public int stock { get; set; }
            public int status { get; set; }
            public int price_max { get; set; }
            public List<Model> models { get; set; }
            public int price { get; set; }
            public object cat_status { get; set; }
            public List<object> tier_variations { get; set; }
            public int touch_time { get; set; }
            public bool is_installment_payment_eligible { get; set; }
            public int third_catid { get; set; }
            public string model_name { get; set; }
        }

        public class Item
        {
            public string comment { get; set; }
            public int rating { get; set; }
            public string author_pic { get; set; }
            public int shopid { get; set; }
            public string author_name { get; set; }
            public int rating_star { get; set; }
            public int mtime { get; set; }
            public object images { get; set; }
            public ItemRatingReply ItemRatingReply { get; set; }
            public int modelid { get; set; }
            public int cmtid { get; set; }
            public int disputable { get; set; }
            public string mentioned { get; set; }
            public int editable_date { get; set; }
            public int authorid { get; set; }
            public List<PreviewItem> preview_items { get; set; }
            public UserRatingReply UserRatingReply { get; set; }
            public int opt { get; set; }
            public int editable { get; set; }
            public bool anonymous { get; set; }
            public string user_pic { get; set; }
            public string username { get; set; }
            public int ctime { get; set; }
            public int itemid { get; set; }
            public List<ProductItem> product_items { get; set; }
            public int userid { get; set; }
            public int filter { get; set; }
            public int status { get; set; }
        }

        public class RootObject
        {
            public List<Item> items { get; set; }
        }


     

    }
}
