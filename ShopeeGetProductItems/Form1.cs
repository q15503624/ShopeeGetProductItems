﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private string _log;
        private int _successCount;

        public Form1()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            _successCount = 0;
            var startTime = DateTime.Now;
            var id = "";
            var itemId = "";
            var error = "";
            if (string.IsNullOrEmpty(tb_shopeeid.Text))
            {
                error = "請輸入[商品連結]\n\r";
            }
            else
            {
                id = GetProductId(id, ref itemId);
            }

            error = CheckError(id, itemId, error);

            if (!string.IsNullOrEmpty(error))
            {
                MessageBox.Show(error, "ERROR");
            }
            else
            {
                await GoGetComment(itemId, id);
                MessageBox.Show($"資料抓取結束。\r\n總筆數:{_successCount}。\r\n花費時間s:{(DateTime.Now - startTime).ToString()}。{_log}", "完成");
            }
        }

        private async Task GoGetComment(string itemId, string id)
        {
            dataGridView1.Rows.Clear();
            button1.Text = "抓資料中...";
            button1.Enabled = false;
            Clear.Enabled = false;
            OutExcel.Enabled = false;
            var sOffset = int.Parse(tb_s_offset.Text);
            var eOffset = int.Parse(tb_e_offset.Text);
            var limit = 10;

            var tasks = new List<Task>();
            while (sOffset < eOffset)
            {
                var url =
                    $@"https://shopee.tw/api/v1/comment_list/?item_id={itemId}&shop_id={id}&offset={sOffset}&limit={limit}&flag=1&filter=0";
                tasks.Add(get_rating(url));
                sOffset += limit;
            }

            await Task.WhenAll(tasks);

            button1.Text = "執行";
            button1.Enabled = true;
            Clear.Enabled = true;
            OutExcel.Enabled = true;
        }

        private string CheckError(string id, string itemId, string error)
        {
            if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(itemId))
            {
                error += "未抓取到商品資訊，請檢查連結是否正確。\n\r";
            }

            if (string.IsNullOrEmpty(tb_s_offset.Text))
            {
                error += "請填[起始筆數]\n\r";
            }

            if (string.IsNullOrEmpty(tb_e_offset.Text))
            {
                error += "請填[結束筆數]\n\r";
            }

            return error;
        }

        private string GetProductId(string id, ref string itemId)
        {
            
            var check = tb_shopeeid.Text.Contains("-i.");

            if (!check)
                return "";

            var keyPoint1 = tb_shopeeid.Text.IndexOf("-i.", StringComparison.Ordinal) + 3 ;
            var keyPoint2 = tb_shopeeid.Text.IndexOf("?", StringComparison.Ordinal);
            var iCode = tb_shopeeid.Text.Substring(keyPoint1, keyPoint2 - keyPoint1);

            var substrings = iCode.Split('.');

            id = substrings[0];
            itemId = substrings[1];

            return id;
        }


        #region 
        public async Task get_rating(string url)
        {
            string request;
            using (var webClient = new WebClient())
            {
                webClient.Encoding = Encoding.UTF8;
                webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                request = await webClient.DownloadStringTaskAsync(new Uri(url));
                webClient.Dispose();
            }

            var rating = JsonConvert.DeserializeObject<dynamic>(request);

            if (rating.comments.Count != 0)
            {
                foreach (var obj in rating.comments)
                {
                    var data = dataGridView1.Rows
                             .OfType<DataGridViewRow>()
                             .Where(x => (string)x.Cells["model_name"].Value == (string)obj.product_items[0].model_name);

                    var repeat = data.Any();
                    if (repeat)
                    {
                        var repeatCell = data.FirstOrDefault().Index;
                        if (repeatCell >= 0 && dataGridView1.Rows.Count > repeatCell) 
                            dataGridView1.Rows[repeatCell].Cells["count"].Value = int.Parse(dataGridView1.Rows[repeatCell].Cells["count"].Value.ToString()) + 1;
                    }
                    else
                    {
                        dataGridView1.Rows.Add(obj.product_items[0].model_name.ToString(), 1);
                    }

                    _successCount++;
                    button1.Text = $"已抓取{_successCount}筆";
                }
            }
        }
        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Clear_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            OutExcel.Enabled = false;
        }

        private void OutExcel_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 1)
            {
                var save = new SaveFileDialog
                {
                    InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                    FileName = DateTime.Now.ToString("yyyymmdd_hhmmss"),
                    Filter = "*.xlsx|*.xlsx"
                };
                if (save.ShowDialog() != DialogResult.OK) return;

                // Excel 物件
                Microsoft.Office.Interop.Excel.Application xls = null;
                try
                {
                    xls = new Microsoft.Office.Interop.Excel.Application();
                    // Excel WorkBook
                    var book = xls.Workbooks.Add();
                    // Excel WorkBook，預設會產生一個 WorkSheet，索引從 1 開始，而非 0
                    // 寫法1
                    var sheet = (Microsoft.Office.Interop.Excel.Worksheet)book.Worksheets.Item[1];

                    // 把 DataGridView 資料塞進 Excel 內
                    DataGridView2Excel(sheet);
                    // 儲存檔案
                    book.SaveAs(save.FileName);
                    MessageBox.Show("資料轉至Excel作業完成", "OK");
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    xls.Quit();
                }
            }
            else
            {
                MessageBox.Show("表單數量有誤!", "ERROR");
            }
        }

        private void DataGridView2Excel(_Worksheet sheet)
        {
            for (var i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                for (var j = 0; j < dataGridView1.Columns.Count; j++) {
                    var value = dataGridView1[j, i].Value.ToString();
                    sheet.Cells[i + 1, j + 1] = value;
                }
            }
        }
       
    }
}
